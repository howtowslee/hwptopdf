var fs = require('fs');
var htmlPdf = require('html-pdf-chrome');
var htmlTemplate = require('angular-template');
var path = "doc08.html";
var options = {
  prefix: 'ng',
  locale: 'ko'
}


var name = 'doc08';

if (process.argv.length == 3) {
  name = process.argv[2];
}

var doc2Obj = {
  row03:{
    year: "2017",
    quarter: "1",
    startMonth: "1",
    startDate: "1",
    endMonth: "6",
    endDate: "30",
  },
  row06:{
    incNumber: "101-01-01010",
    incName: "국제상사테스트",
    상호: "한글테스트",
    userName: "김국세",
    incAddress: "종로구 종로2가 xxx",
    tradeStart: new Date(),
    tradeEnd: new Date(),
    registDate: new Date()
  }
}

var test = "doc02.html";
var dataHtml = htmlTemplate('./'+name+'.html', doc2Obj, options);
fs.writeFileSync('./'+name+'data.html', dataHtml)

var html = fs.readFileSync('./'+name+'data.html', 'utf8');
var commonStyle = fs.readFileSync('./style.css');
var docStyle = fs.readFileSync('./'+name+'.css');
  
html = html.replace('<!--commonStyleHere-->', '<style>'+ commonStyle + '</style>');
html = html.replace('<!--docStyleHere-->',  '<style>'+ docStyle + '</style>');

// fs.writeFileSync('./result.html', html);
htmlPdf.create(html, {
  printOptions: {
    printBackground: true,
    scale: 0.2,
    marginBottom: 0,
    marginTop: 0
  }
})
.then((res) => res.toFile(name+'.pdf'))