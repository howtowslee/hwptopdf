var fs = require('fs');
var htmlPdf = require('html-pdf-chrome');
var 

var name = 'doc08';

if (process.argv.length == 3) {
  name = process.argv[2];
}

var html = fs.readFileSync('./'+name+'.html', 'utf8');
var commonStyle = fs.readFileSync('./style.css');
var docStyle = fs.readFileSync('./'+name+'.css');
  
html = html.replace('<!--commonStyleHere-->', '<style>'+ commonStyle + '</style>');
html = html.replace('<!--docStyleHere-->',  '<style>'+ docStyle + '</style>');

htmlPdf.create(html, {
  printOptions: {
    printBackground: true
  }
})
.then((res) => res.toFile(name+'.pdf'))